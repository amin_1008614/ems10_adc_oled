#include <msp430.h> 
#include <stdint.h>

#pragma vector =  TIMER0_A1_VECTOR
__interrupt void mijnTIMER1_A0InterruptRoutine(void)
{
    ADC10CTL0 |= ADC10SC;
    TA0CTL &= ~ TAIFG; // reset TAIFG
}

#pragma vector = ADC10_VECTOR
__interrupt void mijnADCInterruptRoutine(void)

{
    float static temperatuur = 0;
    temperatuur = ((ADC10MEM / 1023.0) * 1.5) * 1000;
    setTemp((int) temperatuur);
}

void initDisplay();

/* Deze functie past de weer te geven
 * temperatuur aan.
 *
 * int temp: De weer te geven temperatuur.
 * Max is 999, min is -99.
 * Hierbuiten wordt "Err-" weergegeven.
 *
 * Voorbeeld:
 * setTemp(100); // geef 10.0 graden weer
 */
void setTemp(int temp);

/* Deze functie geeft bovenaan in het
 * display een titel weer.
 * char tekst[]: de weer te geven tekst
 *
 * Voorbeeld:
 * setTitle("Hallo"); // geef Hallo weer
 */
void setTitle(char tekst[]);

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // Stop watchdog timer  // Stel klok in op 16MHz
    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ;
    DCOCTL = CALDCO_16MHZ;
    ADC10CTL1 = ADC10DIV_7 | ADC10SSEL_0 | SHS_0 | INCH_5;
    ADC10CTL0 = ADC10ON | SREF_1 | ADC10SHT_3 | ADC10SR | REFON | ADC10IE| REFBURST | ENC;
    ADC10CTL0 |= ADC10IE; // lokale interrupt enable van de ADC10 peripheral

    BCSCTL3 = LFXT1S_2; // zet klok op low frequentie(12kHz)
    TA0CTL = TASSEL_1 | ID_3 | MC_1 | TAIE;
    TA0CCR0 = 749; // tot deze waarde en wordt die gereset

    initDisplay();
    setTitle("Opdracht7.1.13");

    // uint8_t temperatuur = 0; // Fictieve temperatuur
    __enable_interrupt(); // zet interruptsysteem aan

    while (1)
    {

    }
}
